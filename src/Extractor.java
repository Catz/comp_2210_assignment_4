import java.util.*;

/**
 * Extractor.java. Implements feature extraction for collinear points in
 * two dimensional data.
 *
 * @author Dean Hendrix (dh@auburn.edu), Matthew Cather (mrc0033@auburn.edu)
 * @version 2015-09-23
 */
public class Extractor {

    private Point[] points;

    /**
     * resolution settings for stdlib drawing.
     */
    private static int HI_RES = 32768;
    private static int LO_RES = 32;

    /**
     * Builds an extractor based on the points in the
     * file named by filename.
     */
    public Extractor(String filename) {
        In dataStream = new In(filename);
        if (dataStream.exists() && !dataStream.isEmpty()) {
            int numPoints = dataStream.readInt();
            
            points = new Point[numPoints];
            
            for (int i = 0; i < numPoints; i++) {
                points[i] = new Point(dataStream.readInt(), dataStream.readInt());
            }
        }
    }

    /**
     * Builds an extractor based on the points in the
     * Collection named by pcoll.
     */
    public Extractor(Collection<Point> pcoll) {
        if (pcoll != null && !pcoll.isEmpty()) {
            Iterator<Point> data = pcoll.iterator();
            points = new Point[pcoll.size()];
            int i = 0;
            while (data.hasNext()) {
                points[i++] = data.next();
            }
        }
    }

    /**
     * Returns a sorted set of all line segments of exactly four
     * collinear points. Uses a brute-force combinatorial
     * strategy. Returns an empty set if there are no qualifying
     * line segments.
     */
    public SortedSet<Line> getLinesBrute() {
        SortedSet<Line> lines = new TreeSet<Line>();
        Point[] temp = points.clone();

        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < points.length; j++) {
                for (int k = 0; k < points.length; k++) {
                    for (int l = 0; l < points.length; l++) {
                        Line temp2 = new Line();
                        temp2.add(temp[i]);
                        temp2.add(temp[j]);
                        temp2.add(temp[k]);
                        temp2.add(temp[l]);

                        if (temp2.length() == 4){
                            lines.add(temp2);
                        }
                    }
                }
            }
        }
        return lines;
    }

    /**
     * Returns a sorted set of all line segments of at least four
     * collinear points. The line segments are maximal; that is,
     * no sub-segments are identified separately. A sort-and-scan
     * strategy is used. Returns an empty set if there are no qualifying
     * line segments.
     */
    public SortedSet<Line> getLinesFast() {
        SortedSet<Line> output = new TreeSet<Line>(); //output for this method
        for (Point currentPoint : points) { //iterate through all of the points
            Point[] sortedPoints = points.clone();
            Arrays.sort(sortedPoints, currentPoint.SLOPE_ORDER);//sort with respect to currentPoint
            int index = 0;
            while (currentPoint.slopeTo(sortedPoints[index]) == Double.NEGATIVE_INFINITY) { //skip all points equal to current point
                index++;
            }
            while (index < sortedPoints.length) { //iterate though points
                double slope = sortedPoints[0].slopeTo(sortedPoints[index]);//slope of line from current point to sortedPoints[index]
                int temp = coLinear(sortedPoints, index, slope); //finds groups of similar slopes.
                if (temp >= index + 2) { // check that there are three points with the same slope to currentPoint.
                    Line temp2 = new Line();
                    temp2.add(sortedPoints[0]);
                    while (index <= temp) {
                        temp2.add(sortedPoints[index]);
                        index++;
                    }
                    output.add(temp2);
                }
                else index++;
            }
        }
        return output;
    }

    private int coLinear(Point[] array, int index, double desiredSlope) {
        double temp = array[0].slopeTo(array[index]); //slope of line from current point to array[index]
        if ((index >= array.length - 1) && temp == desiredSlope) {
            return index;
        }
        if (index < array.length && temp == desiredSlope) {
            return coLinear(array, index + 1, desiredSlope);
        }
        else return index - 1; // returns when the slope is no longer equal to desired slope.
    }

    public Point[] getPoints() {
        return points;
    }

    // Draw all points to a graphics window.
    public void drawPoints() {
        // optional
    }

    // Draw all identified lines, if any, to a graphics window.
    public void drawLines() {
        // optional
    }

}
