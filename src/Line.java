import java.util.*;

/**
 * Line.java
 * Models a line segment as a Set of Points.
 *
 * @author   Dean Hendrix (dh@auburn.edu), Matthew Cather (mrc0033@auburn.edu)
 * @version  2015-09-30
 *
 */
public class Line implements Comparable<Line>, Iterable<Point> {
 
   private TreeSet<Point> line;
   
   /** Creates a new line with no points. */
   public Line() {
      line = new TreeSet<Point>();
   }
   
   /** Creates a new line with all distinct collinear points in c. */
   public Line(Collection<Point> c) {
      line = new TreeSet<Point>();
      for (Point p : c) {
         add(p);
      }
   }
 
   /** Adds p to this line if distinct and collinear with current points. */
   public void add(Point p) {
      boolean added = false;

      if (length() == 0) {
         added = line.add(p);
      }

      else if (length() == 1){
         added = line.add(p);
      }

      else if (first().slopeTo(last()) == first().slopeTo(p)) {
         added = line.add(p);
      }
   }
   
   /** Returns the first (minimum) point on this line. */
   public Point first() {
      if (length() != 0) {
         return line.first();
      }
      else {
         return null;
      }
   }
   
   /** Returns the last (maximum) point on this line. */
   public Point last() {
      if (line.size() == 0) {
         return null;
      }
      else {
         return line.last();
      }
   }
   
   /** Returns the number of points on this line. */
   public int length() {
      return line.size();
   }
    
  // compare this line to that line
   @Override
   public int compareTo(Line that) {
      int i = this.first().compareTo(that.first());
      int j = this.last().compareTo(that.last());
      if (i == 0) {
         if (j == 0) {
            return 0;
         }
         else if (j < 0) {
            return -1;
         }
         else {
            return 1;
         }
      }
      else if (i < 0) {
         return -1;
      }
      else {
         return 1;
      }
   }
   
  // provide an iterator over all the points on this line
   @Override
   public Iterator<Point> iterator() {
      return line.iterator();
   }
   
   /** 
    * Return true if this point's x and y coordinates are
    * the same as that point's x and y coordinates.
    * Return false otherwise.
    */
   @Override
   public boolean equals(Object obj) {
      if (obj == this) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (!(obj instanceof Line)) {
         return false;
      }
      Line that = (Line) obj;
      return (this.first().equals(that.first())) && (this.last().equals(that.last()));
   }
 
  // return this line as a String
   @Override
   // DO NOT MODIFY
   public String toString() {
      StringBuilder s = new StringBuilder();
      for (Point p : line) {
         s.append(p + " -> ");
      }
      s = s.delete(s.length() - 4, s.length());
      return s.toString();
   }
}
